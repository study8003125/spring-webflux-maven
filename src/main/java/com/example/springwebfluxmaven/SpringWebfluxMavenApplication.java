package com.example.springwebfluxmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebfluxMavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebfluxMavenApplication.class, args);
	}

}
